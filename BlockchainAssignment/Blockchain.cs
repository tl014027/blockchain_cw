﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class Blockchain
    {
        public List<Block> blocks = new List<Block>();

        private DateTime lastBlockTime;
        private int blocksForDifficultyChange = 5;
        private int targetBlockTime = 60;

        private int transactionsPerBlock = 5;

        public List<Transaction> transactionPool = new List<Transaction>();

        public Blockchain()
        {

            blocks = new List<Block>()
            {
                new Block()
            };
            lastBlockTime = DateTime.Now;
        }

        public String GetBlockAsString(int index)
        {
            if (index >= 0 && index < blocks.Count)
                return blocks[index].ToString();
            else
                return "No such block exists";
        }

        public Block GetLastBlock()
        {
            return blocks[blocks.Count - 1];
        }

        private List<Transaction> GetNumberOfTransactions(List<Transaction> sortedTransactions)
        {
            int n = Math.Min(transactionsPerBlock, sortedTransactions.Count);
            return sortedTransactions.GetRange(0, n);
        }


        public static bool ValidateHash(Block b)
        {
            String rehash = b.CreateHash();
            return rehash.Equals(b.hash);
        }

        public static bool ValidateMerkleRoot(Block b)
        {
            String reMerkle = Block.MerkleRoot(b.transactionList);
            return reMerkle.Equals(b.merkleRoot);
        }

        public double GetBalance(String address)
        {
            double balance = 0;

            foreach(Block b in blocks)
            {
                foreach(Transaction t in b.transactionList)
                {
                    if (t.recipientAddress.Equals(address))
                    {
                        balance += t.amount;
                    }
                    if (t.senderAddress.Equals(address))
                    {
                        balance -= (t.amount + t.fee);
                    }
                }
            }
            return balance;
        }

        public override string ToString()
        {
            return String.Join("\n", blocks);
        }




        // Method to change the mining strategy
        public void SetMiningStrategy(MiningStrategy strategy)
        {
            currentMiningStrategy = strategy;
        }

        // Mining strategies. This is selected from the GUI.
        public enum MiningStrategy
        {
            Greedy,
            Altruistic,
            Random,
            PrefAddress,
            Normal
        }

        // Text string for preferred address. Used in the GetPreferredAddressTransactions mining strategy.
        public String preferredAddress = "";

        // Current mining strategy is set to normal - this is the default behaviour.
        public MiningStrategy currentMiningStrategy = MiningStrategy.Normal;


        public List<Transaction> GetPendingTransactions()
        {
            switch (currentMiningStrategy)
            {
                case MiningStrategy.Greedy:
                    return GetGreedyTransactions();
                case MiningStrategy.Altruistic:
                    return GetAltruisticTransactions();
                case MiningStrategy.Random:
                    return GetRandomTransactions();
                case MiningStrategy.PrefAddress:
                    return GetPreferredAddressTransactions();
                case MiningStrategy.Normal:
                    return GetDefaultPendingTransactions();
                default:
                    return GetDefaultPendingTransactions();
            }
        }


        // Gets a list of transactions where the sender or recipient is the preferred address set in the GUI. Ignores other transactions.
        public List<Transaction> GetPreferredAddressTransactions()
        {
            SetMiningStrategy(MiningStrategy.PrefAddress);
            Console.WriteLine("Preferred Address method selected");
            var preferredTransactions = transactionPool.Where(t => preferredAddress.Contains(t.senderAddress) || preferredAddress.Contains(t.recipientAddress)).ToList();
            var otherTransactions = transactionPool.Except(preferredTransactions).ToList();
            preferredTransactions = preferredTransactions.OrderBy(t => t.timestamp).ToList();
            List<Transaction> combinedList = preferredTransactions.Concat(otherTransactions).ToList();
            List<Transaction> transactions = GetNumberOfTransactions(combinedList);
            transactionPool = transactionPool.Except(transactions).ToList();
            return transactions;
        }

        // Orders the pending transactions by timestamp and returns the list.
        public List<Transaction> GetAltruisticTransactions()
        {
            SetMiningStrategy(MiningStrategy.Altruistic);
            Console.WriteLine("Altruistic method selected");
            List<Transaction> sortedTransactions = transactionPool.OrderBy(t => t.timestamp).ToList();
            List<Transaction> transactions = GetNumberOfTransactions(sortedTransactions);
            transactionPool = transactionPool.Except(transactions).ToList();
            return transactions;
        }

        // Orders the pending transactions by fee, sorting by highest first. Returns the list of transactions.
        public List<Transaction> GetGreedyTransactions()
        {
            SetMiningStrategy(MiningStrategy.Greedy);
            Console.WriteLine("Greedy method selected");
            int n = Math.Min(transactionsPerBlock, transactionPool.Count);
            List<Transaction> sortedTransactions = transactionPool.OrderByDescending(t => t.fee).ToList();
            List<Transaction> transactions = sortedTransactions.GetRange(0, n);
            foreach (var transaction in transactions)
            {
                transactionPool.Remove(transaction);
            }
            return transactions;
        }

        // Shuffles the list of pending transactions and returns the list.
        public List<Transaction> GetRandomTransactions()
        {
            SetMiningStrategy(MiningStrategy.Random);
            Console.WriteLine("Random method selected");
            Random rng = new Random();
            List<Transaction> randomizedTransactions = transactionPool.OrderBy(t => rng.Next()).ToList();
            List<Transaction> transactions = GetNumberOfTransactions(randomizedTransactions);
            transactionPool = transactionPool.Except(transactions).ToList();
            return transactions;
        }

        // Gets the list of transactions in the order they were added in.
        public List<Transaction> GetDefaultPendingTransactions()
        {
            SetMiningStrategy(MiningStrategy.Normal);
            Console.WriteLine("Default method selected");
            int n = Math.Min(transactionsPerBlock, transactionPool.Count);

            List<Transaction> transactions = transactionPool.GetRange(0, n);
            transactionPool.RemoveRange(0, n);

            return transactions;
        }

        public List<long> time_to_mine = new List<long>();
        public void SetDifficulty(Block block)
        {
            Stopwatch timer  = new Stopwatch();

                // Starts a timer, mines a block, stops timer and records the time taken in time_to_mine list. Prunes list if more than 10 blocks are in the chain.
                timer.Restart();
                block.Mine();
                timer.Stop();
                long time_taken = timer.ElapsedMilliseconds;
                time_to_mine.Add(time_taken);
                if (time_to_mine.Count() > 5)
                {
                    time_to_mine.Remove(0);
                }

                // Checks if average time to mine a block is longer than the targetted time (60 seconds). Increases for more than the time value, decreases for less than the time value.
                // Only if the block difficulty is less than most_difficult. Initial check ensures time does not go over 1 minute.
                if(time_taken > 60000)
                {
                block.IncreaseDifficulty(false);
                Console.WriteLine($"Block difficulty decreased: {block.GetDifficulty()}");
                }
                else if (time_to_mine.Average() < 24999)
                {
                    block.IncreaseDifficulty(true);
                    Console.WriteLine($"Block difficulty increased: {block.GetDifficulty()}");
                }
                else if (time_to_mine.Average() > 25000)
                {
                    block.IncreaseDifficulty(false);
                    Console.WriteLine($"Block difficulty decreased: {block.GetDifficulty()}");
                }
            
        }

    }
}
